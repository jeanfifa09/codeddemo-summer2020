# Coded Demo Summer 2020 #

Requires use of Visual Studio 2019 (https://visualstudio.microsoft.com/) and the .NET CORE 3.1 SDK (https://dotnet.microsoft.com/download)

### Purpose ###

This is a skills test developed by Coded Inc. for Entry Level Software Developer applicants. Applicants will fork this repository to their own, complete the objective, then provide Coded with access to their repository for review.

### Objective ###

The goal of the project is to receive a JSON payload of property data, modify that data, filter out unnecessary data, then save the result in a text file.  
Use of the provided method and method stubs is optional and comments are appreciated, but not required.  
You are free to use online resources and any Nuget or system libraries you find useful. If you do so, please link your source near the associated code.


The file should have a single line for each property, ordered by property name ascending with these restrictions:  
Property must have at least one unit  
Property image string must be replaced with null if empty  
Null property image string must be represented as 'N/A' in the file  
  
ie: (Image) (Name) (Address)