﻿using System;
using CodedDemoLib;
using CodedDemoLib.Models;
using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace CodedDemo
{
    /* The goal of the project is to receive a JSON payload of property data, modify that data, filter out unnecessary data, then save the result in a text file
     * Use of the provided method and method stubs is optional and comments are appreciated, but not required
     * You are free to use online resources and any Nuget or system libraries you find useful. If you do so, please link your source near the associated code
     * 
     * The file should have a single line for each property, ordered by property name ascending with these restrictions:
     * Property must have at least one unit
     * Property image string must be replaced with null if empty
     * Null property image string must be represented as 'N/A' in the file
     * 
     * ie: (Image) (Name) (Address) */

    class Program
    {
        static void Main(string[] args)
        {
            // This function will give you the property data you need as a JSON formatted string
            string json = Lib.GetPropertyData();

            //Convert data into List
            List<PropertyData> datas = ConvertPropertyData(json);

            //Removing empty images(Modifying)
            RemoveEmptyImages(datas);
            //Ordered by property name ascending 
            List<PropertyData> sortedProperty = datas.OrderBy(property=>property.Name).ToList();

            //Filtering
            List<PropertyData> results= sortedProperty.Where(data => data.Units > 0).ToList();//using System.Linq
            //Generating results(Saving)
            GenerateFile(results);
        }

        // Write a method (function) to convert the JSON payload into the provided model array
       // static PropertyData[] ConvertPropertyData(string json) => throw new NotImplementedException();

        static List<PropertyData> ConvertPropertyData(string json)
        {
            //using Newtonsoft.Json
            return JsonConvert.DeserializeObject<List<PropertyData>>(json); ;
        }
        

        // Method to replace empty image strings with nulls
        // TODO: Fix bug returning empty image strings and null image strings
        static void RemoveEmptyImages(List<PropertyData> propertyData)
        {
            foreach (var p in propertyData)
            {
                p.Image = string.IsNullOrEmpty(p.Image) ? null : p.Image;
                
            }
        }

        // Write a method (function) that creates a text file with the results
        //static void GenerateFile() => throw new NotImplementedException();

        static void GenerateFile(List<PropertyData> properties)
        {
            
            using (StreamWriter sw = new StreamWriter("../../../Output.txt"))
            {

                foreach (PropertyData p in properties)
                {
                    if (p.Image == null)
                    {
                        sw.WriteLine("(N/A) (" + p.Name + ") (" + p.Address + ")");

                    }
                    else
                    {
                        sw.WriteLine("(" + p.Image + ") (" + p.Name + ") (" + p.Address + ")");
                    }
                }
            }

        }
    }
       
}